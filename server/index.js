var mysql = require('mysql')
const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const path= require("path");
var cors = require('cors')

app.use(cors())
app.use(bodyParser.json())

app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

let port = process.env.PORT;
if (port == null || port == "") {
  port = 3000;
}



var dbCreds = {
  host: process.env.DB_HOST || 'localhost',
  user: process.env.DB_USER || 'vishwas',
  password: process.env.DB_PASSWORD || 'Vishwas@1234',
  database: process.env.DB || 'rankingapp',
  multipleStatements: true
};

function getConn(){
  var connection = mysql.createConnection(dbCreds);
  return connection;
}


//HTML ROUTES FOR SINGIN- SIGNUP- HOME
var ui_routes=[{
    route: "/",
    file: "signIn.html"
 },
 {
  route: "/signup",
  file: "signup.html"
},
{
  route: "/home",
  file: "home.html"
}]
for (let i=0;i<ui_routes.length;i++){  
   app.get (ui_routes[i].route,(req,res)=> {
    res.sendFile(getPath(ui_routes[i].file));
  });
}  
function getPath( fileName){
  var a= path.join(__dirname,"..","ui",fileName);
  return a;
}

//API Post request for sign in
app.post ('/auth',(req,res)=> {
  console.log(req.body);
  var connection = getConn();
  var x=req.body.email;
  var y= req.body.password;
  connection.connect()  
    let r = "SELECT id from users where email='"+x+"'AND password='"+y+"'";
    console.log(r);
    connection.query(r, function (err, rows, fields) {
      console.log("Error is "+err);
      console.log("rows is "+rows);
      var f=JSON.stringify(fields);
      console.log("fields is "+ f);
      
      if (err) {
        rows=JSON.stringify({ "error" :"True" });        
      } 
      else{
        rows=JSON.stringify(rows[0]);
      }
        res.send(rows);
      connection.end()
      })
    }
)



//API  POST REQUEST FOR SIGNUP
app.post ('/user',(req,res)=> {
  console.log(req.body);
  var connection = getConn();
  var name=req.body.name;
  var city= req.body.city;
  var edu_det= req.body.edu_det;
  var dob= req.body.dob;
  var email= req.body.email;
  var password1=req.body.password1;
  var gender= req.body.gender;
  var dp_link= req.body.profile_pic;
  var probtotal= parseInt(req.body.probtotal);
  var probsuccess= parseInt(req.body.probsuccess);
  var tot_followers= (req.body.tot_followers);
  var following= parseInt(req.body.following);
  var data_stuc= req.body.data_stuc;
  console.log(tot_followers);
  console.log(data_stuc);
  var algo= req.body.algo;
  var cplus= req.body.cplus;
  var java= req.body.java;
  var python= req.body.python;
  var javascript= req.body.javascript;
  var votes= parseInt(req.body.votes);

  connection.connect()  
    let r = "INSERT INTO users (name, edu_details, dob, gender, email, password, dp_link ) values ('"+name+"','"+edu_det+"','"+dob+"','"+gender+"','"+email+"','"+password1+"','"+dp_link+"'); insert into grades (user_id, solutions_submitted, solutions_accepted, followers, following, data_stuc, algo, cplus, java, python, javascript, votes) values ((select MAX(users.id) from users), "+probtotal+","+probsuccess+","+tot_followers+","+following+","+data_stuc+","+algo+","+cplus+","+java+","+python+","+javascript+","+votes+")";
    //let p= "insert into grades set grades.user_id=(select MAX(users.id) from users)";
    console.log(r);
    connection.query(r,[2,1], function (err, rows, fields) {
      console.log("Error is "+err);
      console.log("rows is "+rows);
      var f=JSON.stringify(fields);
      console.log("fields is "+ f);
      
      if (err) {
        rows=JSON.stringify({ "error" : true });        
      } 
      else{
        rows=JSON.stringify({ "message" :"Success" });
      }
      res.send(rows);
      
      
      connection.end()
        
      })
    }
)

//API GET List of top 3 Rankers
app.get ('/top',(req,res)=> { 
  var connection = getConn();
  connection.connect()  
    let r = "select grades.user_id AS id , users.name AS name , users.dp_link AS pic, DATE_FORMAT(users.dob, '%d %M %Y') as dob, grades.followers as followers from grades inner join users on users.id=grades.user_id order by ((grades.algo+grades.data_stuc+grades.java+grades.javascript+grades.cplus+grades.python)/6) DESC limit 3";
    console.log(r);
    connection.query(r, function (err, rows, fields) {
      console.log("Error is "+err);
      console.log("rows is "+rows);
      var f=JSON.stringify(fields);
      console.log("fields is "+ f);
      
      if (err) {
        rows=JSON.stringify({ "error" :"True" });        
      } 
      else{
        console.log("Date of Birth "+rows[0].dob);
        rows=JSON.stringify(rows);
        }
      res.send(rows);
      connection.end()
        
      })
    }
)

//API GET LIST OF ALL RANKERS ALPHABETICALLY
app.get ('/users',(req,res)=> { 
  var connection = getConn();
  connection.connect()  
    let r = "select users.id AS id, users.name as name, users.dp_link as pic,DATE_FORMAT(users.dob, '%d %M %Y') as dob, grades.followers as followers from users inner join grades on users.id=grades.user_id order by users.name, users.id";
    
    console.log(r);
    connection.query(r, function (err, rows, fields) {
      console.log("Error is "+err);
      console.log("rows is "+rows);
      var f=JSON.stringify(fields);
      console.log("fields is "+ f);
      
      if (err) {
        rows=JSON.stringify({ "error" :"True" });        
      } 
      else{
        
        rows=JSON.stringify(rows);
        }
      res.send(rows);
      connection.end()
        
      })
    }
)

//API GET USER DETAILS OF ALL RANKERS ALPHABETICALLY
app.get ('/details',(req,res)=> { 
  var i= req.query.id;
  var connection = getConn();
  connection.connect()  
    let r = "select * from (select users.id as id, users.name as name, users.dp_link as pic, 'Mumbai' as location, users.edu_details as education, grades.solutions_submitted as challenges_solved, grades.solutions_submitted as solutions_submitted, grades.solutions_accepted as solutions_accepted, rank() over( order by ((grades.java + grades.data_stuc + grades.algo + grades.cplus + grades.python +grades.javascript)/6) DESC ) overall_rank, grades.followers as followers, grades.following as following, grades.data_stuc as data_structures, grades.algo as algorithms, grades.cplus as cplus, grades.java as java, grades.python as python, grades.javascript as javascript, grades.votes as votes from users inner join grades on grades.user_id=users.id) AS temp_rank where temp_rank.id="+i;
    console.log(r);
    connection.query(r, function (err, rows, fields) {
      console.log("Error is "+err);
      console.log("rows is "+rows);
      var f=JSON.stringify(fields);
      console.log("fields is "+ f);
      
      if (err) {
        rows=JSON.stringify({ "error" :"True" });        
      } 
      else{
        
        rows=JSON.stringify(rows[0]);
        }
      res.send(rows);
      connection.end()
        
      })
    }
)









// //Route to Open Group Chat Page

// app.get ('/grouplogin',(req,res)=> {
//   var connection = getConn();


//   var x=req.query.chatgroup;
//   connection.connect()

//   var checkgroup =("select id from chatgroup where id="+x);

//   connection.query( checkgroup, function (err, rows, fields) {
//     //if (err) throw err; 
//     console.log(rows);

//     if (rows.length==0 || err) {
//       // rows={id:"ERROR"};
//       // rows=JSON.stringify(rows);
//       res.send(rows);        
//       res.sendFile(path.join(__dirname,'public',"ChatSuccessDashboard.html"));   
//     }
//     else{
//     rows=JSON.stringify(rows);
//     res.sendFile(path.join(__dirname,'public',"ChatBox.html"))
    

//     connection.end()
//       }
//     })
//  }
// )




// //Now to create an insert request to enter a new message
// app.get ('/newmsg',(req,res)=> {
//   var connection = getConn();


//   var x=req.query.message;
//   x="'"+x+"'";
//   var y= req.query.userid;
//   var z= req.query.groupid;
//   connection.connect()

  
  
    
//   var abc="INSERT INTO msglog (message,time,userid,group_id) VALUES ("+x+",localtimestamp(),"+y+","+z+")";
//   console.log(abc);
    
//     connection.query( abc, function (err, rows, fields) {
//       if (err) throw err; 
//       console.log ("select users.username as username ,msglog.message,msglog.id as msgid as message from msglog inner join users on msglog.userid=users.id where group_id="+z+" order by msglog.id ASC")
//       //console.log ("INSERT INTO msglog (message,time) VALUES ('"+x+"',localtimestamp())");
//       connection.query( "select users.username as username ,msglog.message as message,msglog.id as msgid from msglog inner join users on msglog.userid=users.id where group_id="+z+" order by msglog.id ASC", function (err, rows, fields) {
//         if (err) throw err; 
//         else          
//         res.send(rows);
//         connection.end()
//       })
//     })
//   })

//   //Route to refresh the messaging app
//   app.get ('/refresh',(req,res)=> {
//     var connection = getConn();

  
//     var y= req.query.userid;
//     var z= req.query.groupid;
//     connection.connect()
      
//     var checkLastMessage="SELECT MAX(msglog.id) from msglog inner join chatgroup on msglog.group_id=chatgroup.id where group_id="+z;
//     console.log(checkLastMessage);
      
//       connection.query( checkLastMessage, function (err, rows, fields) {

//         if (err) throw err; 

//         connection.query( "select users.username as username ,msglog.message as message,msglog.id as msgid from msglog inner join users on msglog.userid=users.id where group_id="+z+" order by msglog.id ASC", function (err, rows, fields) {
//           if (err) throw err; 
//           else          
//           res.send(rows);
//           connection.end()
//         })
//       })
//     })
  

  

    

app.listen(port, () => {
console.log(`Example app listening at http://localhost:${port}`)
})